Class {
	#name : #Answer,
	#superclass : #Publication,
	#instVars : [
		'question'
	],
	#category : #'TP-Refactoring-Model'
}

{ #category : #'instance creation' }
Answer class >> description: aDescription user: aUser question: aQuestion [
	^ (self description: aDescription user: aUser)
		initializeQuestion: aQuestion;
		yourself
]

{ #category : #initialize }
Answer >> initializeQuestion: aQuestion [
	question:= aQuestion.
]

{ #category : #private }
Answer >> question: aQuestion [
	question := aQuestion 
]
