Class {
	#name : #CuOOra,
	#superclass : #Object,
	#instVars : [
		'users',
		'topics',
		'questions'
	],
	#category : #'TP-Refactoring-Model'
}

{ #category : #adding }
CuOOra >> addQuestion: aQuestion [
	questions add: aQuestion
]

{ #category : #adding }
CuOOra >> addQuestion: aQuestion forUser: aUser [
	aUser addQuestion: aQuestion.
	self addQuestion: aQuestion.
]

{ #category : #adding }
CuOOra >> addTopic: aTopic [
	topics add: aTopic 

]

{ #category : #adding }
CuOOra >> addUser: aUser [
	users add: aUser 

]

{ #category : #initialize }
CuOOra >> initialize [
	users := OrderedCollection new.
	topics := OrderedCollection new.
	questions := OrderedCollection new
]

{ #category : #accessing }
CuOOra >> questions [
	^ questions
]

{ #category : #utilities }
CuOOra >> todayQuestions [
	^self questions select:[:q | q timestamp asDate = Date today].
]

{ #category : #accessing }
CuOOra >> topics [
	^ topics
]

{ #category : #utilities }
CuOOra >> totalPositives [
	^(self questions sum: [:q | q positiveVotes size ])
]

{ #category : #accessing }
CuOOra >> users [
	^ users
]
