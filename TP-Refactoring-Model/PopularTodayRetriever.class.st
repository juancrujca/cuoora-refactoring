Class {
	#name : #PopularTodayRetriever,
	#superclass : #QuestionRetriever,
	#category : #'TP-Refactoring-Model'
}

{ #category : #retrieving }
PopularTodayRetriever >> questionsForUser: aUser [
|popularTCol averageVotes|
	popularTCol := OrderedCollection new.
	popularTCol := cuoora todayQuestions.
	averageVotes :=(cuoora questions sum: [:q | q positiveVotes size ]) / popularTCol size.
	^(popularTCol select:[:q | q positiveVotes size >= averageVotes ])
]
