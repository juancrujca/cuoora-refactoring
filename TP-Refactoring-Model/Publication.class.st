Class {
	#name : #Publication,
	#superclass : #Object,
	#instVars : [
		'description',
		'votes',
		'timestamp',
		'user'
	],
	#category : #'TP-Refactoring-Model'
}

{ #category : #'instance creation' }
Publication class >> description: aDescription  user: aUser [
	^ self new initializeUser: aUser description: aDescription.
		
]

{ #category : #adding }
Publication >> addVote: aVote [
	votes add: aVote
]

{ #category : #accessing }
Publication >> description [
	^ description
]

{ #category : #private }
Publication >> description: aDescription [ 
	description := aDescription
]

{ #category : #initialize }
Publication >> initialize [
	votes := OrderedCollection new.
	timestamp := DateAndTime now.
]

{ #category : #initialize }
Publication >> initializeUser: aUser description: aPublication [
	user:= aUser.
	description:= aPublication.
]

{ #category : #'getting votes' }
Publication >> negativeVotes [
	^votes reject:[:vote | vote isLike].
]

{ #category : #'getting votes' }
Publication >> positiveVotes [
	^votes select:[:vote | vote isLike].
	
]

{ #category : #accessing }
Publication >> timestamp [
	^ timestamp
]

{ #category : #private }
Publication >> timestamp: aTimestamp [
	timestamp := aTimestamp
]

{ #category : #accessing }
Publication >> user [
	^ user
]

{ #category : #private }
Publication >> user: aUser [
	user := aUser
]

{ #category : #accessing }
Publication >> votes [
	^ votes
]
