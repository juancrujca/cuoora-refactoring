Class {
	#name : #Question,
	#superclass : #Publication,
	#instVars : [
		'title',
		'answers',
		'topics'
	],
	#category : #'TP-Refactoring-Model'
}

{ #category : #'instance creation' }
Question class >> title: aTitle description: aDescription user: aUser [
	^ (self description: aDescription  user: aUser)
		initializeTitle: aTitle;
		yourself.
]

{ #category : #'instance creation' }
Question class >> title: aTitle description: aDescription user: aUser topic: aTopic [
	^ (self description: aDescription  user: aUser)
		initializeTitle: aTitle addTopic: aTopic;
		yourself.
]

{ #category : #adding }
Question >> addTopic: aTopic [
	topics add: aTopic.
	aTopic addQuestion: self.

]

{ #category : #comparing }
Question >> compareUser:aUser [
	^(self user=aUser)
]

{ #category : #initialize }
Question >> initialize [
	super initialize.
	answers := OrderedCollection new.
	topics := OrderedCollection new.
]

{ #category : #initialize }
Question >> initializeTitle: aTitle [
	title:= aTitle.
]

{ #category : #initialize }
Question >> initializeTitle: aTitle addTopic: aTopic [
	title:= aTitle.
	self addTopic: aTopic.
]

{ #category : #accessing }
Question >> title [
	^title 
]

{ #category : #accessing }
Question >> title: aTitle [
	title := aTitle 
]

{ #category : #accessing }
Question >> topics [
	^topics 
]
