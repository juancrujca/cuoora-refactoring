Class {
	#name : #QuestionRetriever,
	#superclass : #Object,
	#instVars : [
		'cuoora'
	],
	#category : #'TP-Refactoring-Model'
}

{ #category : #'instance creation' }
QuestionRetriever class >> new: cuoora [
	^ self new cuoora: cuoora
]

{ #category : #accesing }
QuestionRetriever >> cuoora: aCuooraInstance [
	cuoora := aCuooraInstance 
]

{ #category : #retrieving }
QuestionRetriever >> last: amount from: questions [
	^questions last:(amount min:questions size).
]

{ #category : #retrieving }
QuestionRetriever >> questionsForUser: aUser [
	^self subclassResponsibility. 
]

{ #category : #retrieving }
QuestionRetriever >> retrieveQuestions: aUser [
	^(self last: 100 from: (self sortQuestionsForUser: aUser)) reject:[:q|q compareUser:aUser].
]

{ #category : #retrieving }
QuestionRetriever >> sortQuestionsForUser: aUser [
	^(self questionsForUser: aUser) asSortedCollection:[ :a :b | a positiveVotes size > b positiveVotes size ].
]
