Class {
	#name : #TopicsRetriever,
	#superclass : #QuestionRetriever,
	#category : #'TP-Refactoring-Model'
}

{ #category : #retrieving }
TopicsRetriever >> questionsForUser:aUser [
	^(aUser topicsQuestions).
]
