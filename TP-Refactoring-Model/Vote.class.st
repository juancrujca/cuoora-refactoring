Class {
	#name : #Vote,
	#superclass : #Object,
	#instVars : [
		'isLike',
		'timestamp',
		'publication',
		'user'
	],
	#category : #'TP-Refactoring-Model'
}

{ #category : #'instance creation' }
Vote class >> user: aUser dislikesPublication: aPublication [
	^ self new initializeUser: aUser publication: aPublication;
		dislike;
		yourself
]

{ #category : #'instance creation' }
Vote class >> user: aUser likesPublication: aPublication [
	^ self new initializeUser: aUser publication: aPublication
]

{ #category : #private }
Vote >> dislike [
	isLike := false.
]

{ #category : #initialize }
Vote >> initialize [
	isLike := true.
	timestamp := DateAndTime now.
	
]

{ #category : #initialize }
Vote >> initializeUser: aUser publication: aPublication [
	user := aUser.
	publication := aPublication.
]

{ #category : #accessing }
Vote >> isLike [
	^ isLike
]

{ #category : #private }
Vote >> like [
	isLike := true.
]

{ #category : #accessing }
Vote >> publication [
	^ publication
]

{ #category : #private }
Vote >> publication: aPublication [
	publication := aPublication
]

{ #category : #accessing }
Vote >> user [
	^ user
]

{ #category : #private }
Vote >> user: aUser [
	user := aUser
]
