Class {
	#name : #PublicationTest,
	#superclass : #TestCase,
	#instVars : [
		'publication'
	],
	#category : #'TP-Refactoring-Tests'
}

{ #category : #setup }
PublicationTest >> setUp [	
	publication := Publication description: 'A description'  user:(User new).
]

{ #category : #testing }
PublicationTest >> testNegativeVotes [		
	publication addVote: (Vote user: (User new) dislikesPublication: publication).
	self assert: (publication negativeVotes size) equals: 1.
	
	publication addVote: (Vote user: (User new) dislikesPublication: publication).
	self assert: (publication negativeVotes size) equals: 2.

]

{ #category : #testing }
PublicationTest >> testPositiveAndNegativeVotesInterference [
	
	publication addVote: (Vote user: (User new) dislikesPublication: publication).
	self assert: (publication positiveVotes size) equals: 0.
	
	publication addVote: (Vote user: (User new) likesPublication: publication).	
	self assert: (publication positiveVotes size) equals: 1.
	
	publication addVote: (Vote user: (User new) likesPublication: publication).	
	self assert: (publication positiveVotes size) equals: 2.
	
	publication addVote: (Vote user: (User new) dislikesPublication: publication).
	self assert: (publication positiveVotes size) equals: 2.
	
	self assert: publication votes size equals: 4.
]

{ #category : #testing }
PublicationTest >> testPositiveVotes [
	publication addVote: (Vote user: (User new) likesPublication: publication).	
	self assert: (publication positiveVotes size) equals: 1.
	
	publication addVote: (Vote user: (User new) likesPublication: publication).	
	self assert: (publication positiveVotes size) equals: 2.
	
]

{ #category : #testing }
PublicationTest >> testVotesForNewPublication [
	self assert: (publication negativeVotes size) equals: 0.
	self assert: (publication positiveVotes size) equals: 0.
		
]
